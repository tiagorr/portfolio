import {ChakraProvider, Center} from "@chakra-ui/react";

function MyApp({Component, pageProps}) {
	return (
		<ChakraProvider>
			<Center w="100%" h="100vh" backgroundImage="url(/assets/bg-tile.png)">
				<Component {...pageProps} />
			</Center>
		</ChakraProvider>
	);
}

export default MyApp;
