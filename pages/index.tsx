import {Avatar, Box, Button, Center, Divider, Flex, Heading, HStack, Image, Link, Stack, Text} from "@chakra-ui/react";
import {FaGithub, FaBitbucket, FaAddressCard, FaNewspaper, FaLinkedin, FaEnvelopeOpenText} from "react-icons/fa";
import {ILink} from "@interfaces";
import Head from "next/head";

const links: ILink[] = [
	{label: "About", url: "/about", icon: FaAddressCard, colorScheme: "gray"},
	{label: "Curriculum Vitae", url: "/cv", icon: FaNewspaper, colorScheme: "gray"},
	{label: "Contact", url: "/contact", icon: FaEnvelopeOpenText, colorScheme: "gray"},
];

const outsideLinks: ILink[] = [
	{label: "Github", url: "https://github.com/tiago-rr", icon: FaGithub, colorScheme: "black"},
	{label: "Bitbucket", url: "https://bitbucket.org/tiagorr/", icon: FaBitbucket, colorScheme: "blue"},
	{label: "LinkedIn", url: "https://linkedin.com/in/tiago-err", icon: FaLinkedin, colorScheme: "cyan"},
];

function Home() {
	return (
		<Box
			h={["100%", "100%", "80%", "80%", "80%", "70%"]}
			w={["100%", "80%", "60%", "50%", "30%"]}
			backgroundColor="white"
			rounded={["0", "10"]}
			p={["28px", "32px", "36px", "48px"]}>
			<Head>
				<title>Tiago Ribeiro</title>
			</Head>
			<Flex w="100%" h="100%" alignItems="center" direction="column" justifyContent="space-between">
				<Stack spacing={["12px", "12px", "16px", "20px"]} mb={["12px", "16px", "16px", "24px"]} alignItems="center">
					<Avatar src="/assets/avatar.jpeg" size="2xl" />
					<Stack spacing={["2px", "4px", "6px"]} alignItems="center">
						<Heading>Tiago Ribeiro</Heading>
						<Text>Web Developer</Text>
					</Stack>
				</Stack>
				<Center h="100%">
					<Stack spacing={["8px", "12px", "16px", "20px"]} w="100%" h={["90%", "80%", "80%", "75%"]}>
						{links.map((item) => (
							<Link href={item.url} key={item.label.toLowerCase()} w="250px">
								<Button colorScheme={item.colorScheme} leftIcon={<item.icon />} w="100%">
									{item.label}
								</Button>
							</Link>
						))}
						<Divider />
						{outsideLinks.map((item) => (
							<Link href={item.url} key={item.label.toLowerCase()} target="_blank" w="250px">
								<Button colorScheme={item.colorScheme} variant="outline" leftIcon={<item.icon />} w="100%">
									{item.label}
								</Button>
							</Link>
						))}
					</Stack>
				</Center>
			</Flex>
		</Box>
	);
}

export default Home;
