import {Box, Text, Flex, Heading, Link, Button, Stack, HStack, Avatar} from "@chakra-ui/react";
import aboutFields from "../aboutFields.json";
import Head from "next/head";
import moment from "moment";

function About() {
	const age = moment().diff(new Date(aboutFields.birthDate), "years");

	return (
		<Box
			h={["100%", "100%", "90%", "90%", "80%", "80%"]}
			w={["100%", "100%", "90%", "90%", "80%", "80%"]}
			backgroundColor="white"
			rounded={["0", "10"]}
			p={["28px", "32px", "36px", "48px"]}>
			<Head>
				<title>About - Tiago Ribeiro</title>
			</Head>
			<Flex direction="column" alignItems="center" justifyContent="space-between" h="100%">
				<Heading mb={["12px", "16px", "16px", "24px"]}>About</Heading>
				<Stack h="100%" w="100%">
					<HStack spacing="24px">
						<Avatar src="/assets/avatar.jpeg" size="2xl" />
						<Stack>
							<Text>{aboutFields.name}</Text>
							<Text>{age} years old</Text>
						</Stack>
					</HStack>
				</Stack>
				<Link href="/">
					<Button>Home Page</Button>
				</Link>
			</Flex>
		</Box>
	);
}

export default About;
