import {IconType} from "react-icons";

export interface ILink {
	label: string;
	url: string;
	colorScheme: string;
	icon: IconType;
}

export interface IIcon {
	url: string;
	src: string;
}
